/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "genetique.h"

float budget(Graphe g, int* mesures_arc, Mesure mesures, int arc) {
    if(g.arcs[arc].val != 0)
        return mesures.cout[mesures_arc[arc]];
    return 0;
}

float budgetTotal(Graphe g, int* mesures_arc, Mesure mesures) {
    int tot = 0;
    int i;
    for(i = 0; i < g.M; i++)
        tot += budget(g, mesures_arc, mesures, i);
    return tot;
}

int longueur(Graphe g) {
    //return pcc(g);
}