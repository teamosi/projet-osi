/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   genetique.h
 * Author: blackbern
 *
 * Created on 22 février 2016, 10:15
 */

#ifndef GENETIQUE_H
#define GENETIQUE_H

#include <time.h>
#include "graphe.h"
#include "Budget.h"
#include "mesure.h"

/**
 * 
 * @param P : Le pere
 * @param M : La mere
 * @param pos : l'indice de separation
 */
Graphe* croiser(Graphe P, Graphe M, int pos);

int P;
float BUDGETCIBLE;

void setP(int ponderation);
void setBudgetCible(int budgetCible);

float budget(Graphe g, int* mesures_arc, Mesure mesures, int arc);

float budgetTotal(Graphe g, int* mesures_arc, Mesure mesures);

int longueur(Graphe g);

int valeur(Graphe graphe, Graphe graphe_depart, int* mesures_arc, Mesure mesures);

int ajoutGraphe(Graphe graphe);

Graphe* roueDeLaFortune(Graphe* G, Graphe graphe_depart, int nbGraphe, int* mesures_arc, Mesure mesures, int n);

#endif /* GENETIQUE_H */

