/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   mutation.h
 * Author: thomas
 *
 * Created on 21 mars 2016, 16:24
 */

#ifndef MUTATION_H
#define MUTATION_H

#include <time.h>
#include "graphe.h"
#include "Budget.h"
#include "mesure.h"


Graphe* muterListe(Graphe* v, int nbGraphe, int proba, int* mesures_arc, Mesure mesures);

Graphe muter(Graphe vi, int* mesures_arc, Mesure mesures);

#endif /* MUTATION_H */

