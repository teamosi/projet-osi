/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Tests.h
 * Author: blackbern
 *
 * Created on 18 mars 2016, 14:16
 */

#ifndef TESTS_H
#define TESTS_H

#include "graphe.h"
#include "Budget.h"
#include "genetique.h"
#include <time.h>

Graphe test_graphe();
void test_decomposition(Graphe g);
void test_croisement(Graphe P, Graphe M);

#endif /* TESTS_H */

