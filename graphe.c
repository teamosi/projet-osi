/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "graphe.h"

#define max(a,b) (a>b?a:b)
#define min(a,b) (a<b?a:b)

Arc creer_arc(int Nd, int Na, int V) {
    Arc res;
    res.Na = Na;
    res.Nd = Nd;
    res.val = V;
    return res;
}

Graphe creer_graphe(int N) {
    Graphe g;
    int i, j;
    g.N = N;
    g.M = 0;
    g.arcs = NULL;
    g.pred = (int**)malloc((N+1)*sizeof(int*));
    for(i = 0; i < N+1; i++) {
        g.pred[i] = (int*)malloc((N+1)*sizeof(int));
        for(j = 0; j < N+1; j++) {
            g.pred[i][j] = -1;
        }
    }
    return g;
}

Graphe ajouter_arc(Graphe g, int N1, int N2, int val) {
    if(N1 <= g.N && N2 <= g.N) {
        // ajout dans la liste des predecesseurs
        int *p = g.pred[N2];
        while(*p != -1) p++;
        *p = N1;

        // ajout dans la liste des arcs
        Arc ajout;
        ajout.Nd = N1;
        ajout.Na = N2;
        ajout.val = val;
        g = ajouter_arc_2(g, ajout);
    }
    return g;
}

Graphe ajouter_arc_2(Graphe g, Arc a) {
    if(a.Na <= g.N && a.Nd <= g.N) {
        // ajout dans la liste des predecesseurs
        int *p = g.pred[a.Na];
        while(*p != -1 && *p != a.Nd) p++;
        *p = a.Nd;

        // ajout dans la liste des arcs
        g.M++;
        g.arcs = realloc(g.arcs, g.M*sizeof(Arc));
        g.arcs[g.M-1] = a;
    }
    return g;
}

Graphe supprimer_arc(Graphe g, int pos) {
    int i = 0;
    Arc a = g.arcs[pos];
    
    while(g.pred[a.Na][i] != a.Nd) i++;
    for(; i < g.N; i++){
        g.pred[a.Na][i] = g.pred[a.Na][i+1];
    }
    
    for(i = pos; i < g.M-1; i++){
        g.arcs[i] = g.arcs[i+1];
    }
    g.M--;
    return g;
}

void afficher_arcs(Graphe g){
    int i;
    Arc *p = g.arcs;
    for(i = 0; i < g.M; i++) {
        printf("%d -> %d : %d\n", p[i].Nd, p[i].Na, p[i].val);
    }
}

int** decomposition_cycle(Graphe g) {
    Arc a;
    int i, j, nb_sp, nb_niv = 0, pad, existe;
    int **niveaux = (int**)malloc((g.N+2) * sizeof(int*)); // liste des niveaux
    int *sans_pred; // liste des noeuds sans predecesseur
    int *suppr, *noeuds;  // listes permettant de gérer la suppression d'arcs
    // listes pour faire le lien entre les nouveaux numéros de sommet et les anciens
    int *liens = (int*)malloc((g.N + 1) * sizeof(int)), *liens_tmp = (int*)malloc((g.N + 1) * sizeof(int));
    int *distances = (int*)malloc((g.N + 1) * sizeof(int)); // distances vers les noeuds pour le pcc
    Graphe g_tmp = g, tmp; // graphes de travail
    
    for(i = 0; i < g.N+1; i++){
        niveaux[i] = NULL;
        distances[i] = -1;
        liens[i] = i;
    }
    liens[g.N] = -1;
    niveaux[i] = NULL;
    for(nb_niv = 0; nb_niv < g.N; nb_niv++){
        // recherche des noeuds sans prédecesseur
        sans_pred = NULL;
        nb_sp = 0;
        for(i = 0; i < g_tmp.N; i++){
            if(g_tmp.pred[i][0] == -1){
                existe = 0;
                for(j = 0; j < nb_sp; j++){
                    if(sans_pred[j] == i)
                        existe = 1;
                }
                if(!existe){
                    sans_pred = realloc(sans_pred, (++nb_sp) * sizeof(int));
                    sans_pred[nb_sp-1] = i;
                    if(nb_niv == 0){
                        distances[i] = 0; // initialisation des distances
                        liens[i] = i;
                    }
                }
            }
        }

        // le graphe et sans_pred sont vides
        if(g_tmp.N == 0 && nb_sp == 0) {
            niveaux[nb_niv] = distances;
            return niveaux;
        }

        // seul sans_pred est vide
        if(nb_sp == 0) {
            return NULL;
        }

        // aucun des deux n'est vide
        // stockage du niveau 'nb_niv'
        niveaux[nb_niv] = (int*)malloc((nb_sp+1)*sizeof(int));
        for(i = 0; i < nb_sp; i++) {
            niveaux[nb_niv][i] = liens[sans_pred[i]];
        }
        niveaux[nb_niv][i] = -1;
        
        // deconnexion des arcs de sans_pred
        suppr = (int*)malloc(g_tmp.M*sizeof(int));
        noeuds = (int*)malloc(g_tmp.N*sizeof(int));
        for(i = 0; i < g_tmp.M; i++) {
            suppr[i] = 0;
        }
        for(i = 0; i < g_tmp.N; i++) {
            noeuds[i] = -1;
        }
        // liste des arcs à supprimer
        for(i = 0; i < nb_sp; i++){
            for(j = 0; j < g_tmp.M; j++) {
                a = g_tmp.arcs[j];
                if(a.Nd == sans_pred[i]){
                    if(distances[liens[a.Na]] == -1) distances[liens[a.Na]] = distances[liens[a.Nd]] + a.val;
                    else distances[liens[a.Na]] = min(distances[liens[a.Na]], distances[liens[a.Nd]] + a.val);
                    suppr[j] = 1;
                }
            }
        }
        // suppression des arcs
        pad = 0;
        tmp = g_tmp;
        for(i = 0; i < g_tmp.M; i++) {
            if(suppr[i] == 1)
                tmp = supprimer_arc(tmp, i-pad++);
        }
        g_tmp = tmp;
        pad = 0;
        
        // création du nouveau graphe
        tmp = creer_graphe(g_tmp.N - nb_sp);
        // copie des liens
        for(i = 0; i < g.N; i++) {
            liens_tmp[i] = liens[i];
        }
        for(i = 0; i < g_tmp.M; i++) {
            // changement des numéros de sommet
            a = g_tmp.arcs[i];
            if(noeuds[a.Nd] == -1){
                noeuds[a.Nd] = pad++;
                liens_tmp[pad-1] = liens[a.Nd];
            }
            if(noeuds[a.Na] == -1){
                noeuds[a.Na] = pad++;
                liens_tmp[pad-1] = liens[a.Na];
            }
            tmp = ajouter_arc(tmp, noeuds[a.Nd], noeuds[a.Na], a.val);
        }
        liens = liens_tmp;
        g_tmp = tmp;
    }
}

int pcc() {
    return 0;
}