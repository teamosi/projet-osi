/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   graphe.h
 * Author: blackbern
 *
 * Created on 18 janvier 2016, 12:10
 */

#ifndef GRAPHE_H
#define GRAPHE_H

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

typedef struct Arc{
    int Nd;
    int Na;
    int val;
} Arc;

typedef struct Graphe{
    int N;
    int M;
    Arc *arcs;
    int **pred;
} Graphe;

typedef struct BF_sol {
    int **dist;
    int *peres;
} BF_sol;

/**
 * 
 * @param Nd : Noeud depart
 * @param Na : Noeud destination
 * @param V : Valuation de l'arc
 * @return un arc Nd->Na:V
 */
Arc creer_arc(int Nd, int Na, int V);

/**
 * methode pour construire un graphe de N noeuds sans arcs.
 * @param N : nombre de noeuds dans le graphe
 * @return un graphe de N noeuds
 */
Graphe creer_graphe(int N);

/**
 * methode pour ajouter un arc dans un graphe
 * @param g : graphe a modifier
 * @param N1 : noeud origine
 * @param N2 : noeud destination
 * @param val : valuation de l'arc
 * @return le graphe g modifie
 */
Graphe ajouter_arc(Graphe g, int N1, int N2, int val);
Graphe ajouter_arc_2(Graphe g, Arc a);

/**
 * methode pour supprimer un arc dans un graphe
 * @param g : le graphe a modifier
 * @param pos : la position dans la liste de l'arc a supprimer
 * return le graphe modifie
 */
Graphe supprimer_arc(Graphe g, int pos);

/**
 * fonction qui affiche les arcs d'un graphe
 * @param g : le graphe a afficher
 */
void afficher_arcs(Graphe g);

/**
 * 
 * @param g : le graphe a decomposer
 * @return: un tableau des niveaux ou NULL s'il y a un cycle
 */
int** decomposition_cycle(Graphe g);

int pcc();

#endif /* GRAPHE_H */
