#include "tests.h"

#define max(a,b) (a>b?a:b)
#define min(a,b) (a<b?a:b)

Graphe test_graphe(){
    int i;
    Graphe g = creer_graphe(10); // création d'un graphe de 10 noeuds
    
    // Ajout des arcs
    g = ajouter_arc(g, 0, 1, 85);
    g = ajouter_arc(g, 0, 2, 217);
    g = ajouter_arc(g, 0, 3, 13);
    g = ajouter_arc(g, 0, 4, 173);
    g = ajouter_arc(g, 1, 5, 80);
    g = ajouter_arc(g, 2, 6, 186);
    g = ajouter_arc(g, 2, 7, 103);
    g = ajouter_arc(g, 4, 9, 502);
    g = ajouter_arc(g, 5, 8, 250);
    g = ajouter_arc(g, 7, 3, 183);
    g = ajouter_arc(g, 7, 9, 167);
    g = ajouter_arc(g, 8, 9, 84);
    
    // Affichage des arcs
    afficher_arcs(g);
    
    printf("nombre de noeuds : %d\n", g.N);
    printf("nombre d'arcs : %d\n", g.M);
    
    printf("----------- SUPPR ----------\n");
    // suppression d'un arc
    g = supprimer_arc(g, 2);
    afficher_arcs(g);
    printf("nombre d'arcs : %d\n", g.M);
    
    printf("----------- PRED ----------\n");
    for(i = 0; i < g.N; i++){
        printf("%d\n", g.pred[4][i]);
    }
    
    return g;
}

void test_decomposition(Graphe g){
    int **sol = decomposition_cycle(g);
    int **p1 = sol, *p2;
    while(*p1 != NULL) {
        printf("--- Niveau %d ---\n", (p1-sol));
        p2 = *p1;
        while(*p2 != -1) {
            printf("%d ", *p2);
            p2++;
        }
        p1++;
        printf("\n");
    }
}

void test_croisement(Graphe P, Graphe M) {
    Graphe *croisement;
    
    srand(time(NULL));
    croisement = croiser(P, M, rand() % (max(P.N,M.N)));
    
    printf("--- PERE ---\n");
    afficher_arcs(P);
    
    printf("--- MERE ---\n");
    afficher_arcs(M);
    
    printf("--- FILS ---\n");
    afficher_arcs(croisement[0]);
    
    printf("--- FILLE ---\n");
    afficher_arcs(croisement[1]);
}