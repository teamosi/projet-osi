/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   mesure.h
 * Author: Panzers
 *
 * Created on 22 février 2016, 11:52
 */

#ifndef MESURE_H
#define MESURE_H

#include <stdlib.h>
#include <string.h>

typedef struct mesure {
    char** nom;
    float* cout;
    int nombreMesure;
} Mesure;

/**
 * methode d'ajout d'une mesure.
 * @param mesures : liste des mesures
 * @param nouveauNom : nom de la mesure à ajouter
 * @param nouveauCout : coût de la nouvelle mesure
 * @return la liste des mesures avec la mesure ajoutée
 */
Mesure* ajoutMesure(Mesure* mesures, char* nouveauNom, float nouveauCout);

/**
 * methode pour connaitre le coût d'une mesure entrée en paramètre
 * @param mesures : liste des mesures
 * @param nom : nom de la mesure
 * @return le coût de la mesure
 */
float getCout(Mesure* mesures, char* nom);

#endif /* MESURE_H */