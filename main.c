/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: blackbern
 *
 * Created on 18 janvier 2016, 11:46
 */

#include <stdio.h>
#include <stdlib.h>

#include "Budget.h"
#include "graphe.h"
#include "genetique.h"
#include "mesure.h"
#include "mutation.h"

#define max(a,b) (a>b?a:b)
#define min(a,b) (a<b?a:b)
/*
 * 
 */

int main(int argc, char** argv) {
    FILE *fichier_graphe = fopen("./graphe.txt", "r");
    FILE *fichier_mesure = fopen("./mesures.txt", "r");
    FILE *fichier_asso = fopen("./asso.txt", "r");
    char buffer[BUFSIZ], nom_mesure[BUFSIZ], c;
    int N, depart, arrivee, valeur, nb_gen, i, j, p, nb;
    int *mesure_asso = NULL;
    float cout_mesure, budget_cible, facteur_p;
    Graphe g, pere, mere, *graphes, *enfants;
    Mesure* mesures;
    
    srand(time(NULL));
    
    // construction du graphe
    fgets(buffer, BUFSIZ, fichier_graphe);
    N = atoi(buffer);
    g = creer_graphe(N);
    while(fscanf(fichier_graphe, "%d %d %d\n", &depart, &arrivee, &valeur) != EOF) {
        g = ajouter_arc(g, depart, arrivee, valeur);
    }
    afficher_arcs(g);
    fclose(fichier_graphe);
    
    // récupération des mesures
    mesures = (Mesure*)malloc(sizeof(Mesure));
    mesures->nombreMesure = 0;
    mesures->nom = NULL;
    mesures->cout = NULL;
    while(fscanf(fichier_mesure, "%s %d\n", nom_mesure, &cout_mesure) != EOF) {
        mesures = ajoutMesure(mesures, nom_mesure, cout_mesure);
    }
    
    // association entre arc et mesure
    mesure_asso = (int*)malloc(g.M * sizeof(int));
    while(fscanf(fichier_asso, "%d %d\n", &i, &j) != EOF) {
        mesure_asso[i] = j;
    }
    
    // saisie des autres paramètres
    printf("Veuillez saisir le budget cible : ");
    scanf("%d\n", &budget_cible);
    
    printf("Combien de generation souhaitez vous créer ? ");
    scanf("%d\n", &nb_gen);
    
    printf("Quel est le facteur budget/longueur ? ");
    scanf("%d\n", &facteur_p);
    
    printf("Combien de solutions ? ");
    scanf("%d\n", &nb);
    
    // mutations
    N = rand()%g.N + 1;
    graphes = (Graphe*)malloc(N * sizeof(Graphe));
    for(i = 0; i < N; i++) {
        printf("%d\n", i);
        graphes[i] = muter(g, mesure_asso, *mesures);
    }
    
    // generation des generations
    for(i = 0; i < nb_gen; i++) {
        pere = graphes[rand()%N];
        mere = graphes[rand()%N];
        enfants = croiser(pere, mere, rand() % (max(pere.N,mere.N)));
        printf("Quelle est la probabilite ? ");
        scanf("%d\n", &p);
        enfants = muterListe(enfants, 2, p, mesure_asso, *mesures);
        roueDeLaFortune(enfants, g, 2, mesure_asso, *mesures, nb);
    }
    
    return (EXIT_SUCCESS);
}
