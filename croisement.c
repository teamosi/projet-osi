/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "genetique.h"

#define max(a,b) (a>b?a:b)
#define min(a,b) (a<b?a:b)

Graphe* croiser(Graphe P, Graphe M, int pos) {
    int i, nbs_fils = 0, nbs_fille = 0;
    Graphe fils, fille, *res;
    int *sommets_fils = (int*)malloc(max(P.N, M.N)*sizeof(int));
    int *sommets_fille = (int*)malloc(max(P.N, M.N)*sizeof(int));
    Arc a;
    
    for(i = 0; i < max(P.N, M.N); i++) {
        sommets_fils[i] = 0;
        sommets_fille[i] = 0;
    }
    
    // calcul du nombre de sommets dans le fils
    for(i = 0; i < P.M && i < pos; i++) {
        printf(" i : %d\n", i);
        a = P.arcs[i];
        if(sommets_fils[a.Na] == 0) {
            sommets_fils[a.Na] = 1;
            nbs_fils++;
        }
        if(sommets_fils[a.Nd] == 0) {
            sommets_fils[a.Nd] = 1;
            nbs_fils++;
        }
    }
    for(; i < M.M; i++) {
        printf(" i : %d\n", i);
        a = M.arcs[i];
        if(sommets_fils[a.Na] == 0) {
            sommets_fils[a.Na] = 1;
            nbs_fils++;
        }
        if(sommets_fils[a.Nd] == 0) {
            sommets_fils[a.Nd] = 1;
            nbs_fils++;
        }
    }
    printf("sommets fils : %d\n", nbs_fils);
    // calcul du nombre de sommets dans la fille
    for(i = 0; i < M.M && i < pos; i++) {
        printf(" i : %d\n", i);
        a = M.arcs[i];
        if(sommets_fille[a.Na] == 0) {
            sommets_fille[a.Na] = 1;
            nbs_fille++;
        }
        if(sommets_fille[a.Nd] == 0) {
            sommets_fille[a.Nd] = 1;
            nbs_fille++;
        }
    }
    for(; i < P.M; i++) {
        printf(" i : %d\n", i);
        a = P.arcs[i];
        if(sommets_fille[a.Na] == 0) {
            sommets_fille[a.Na] = 1;
            nbs_fille++;
        }
        if(sommets_fille[a.Nd] == 0) {
            sommets_fille[a.Nd] = 1;
            nbs_fille++;
        }
    }
    printf("sommets fille : %d\n", nbs_fille);
    //création des graphes
    fille = creer_graphe(nbs_fille);
    fils = creer_graphe(nbs_fils);
    
    //copie des arcs pour le fils
    for(i = 0; i < P.M && i < pos; i++) {
        fils = ajouter_arc_2(fils, P.arcs[i]);
    }
    for(; i < M.M; i++) {
        fils = ajouter_arc_2(fils, M.arcs[i]);
    }
    
    //copie des arcs pour la fille
    for(i = 0; i < M.M && i < pos; i++) {
        fille = ajouter_arc_2(fille, M.arcs[i]);
    }
    for(; i < P.M; i++) {
        fille = ajouter_arc_2(fille, P.arcs[i]);
    }
    
    // création du couple et retour
    res = malloc(2 * sizeof(Graphe));
    res[0] = fils;
    res[1] = fille;
    return res;
}