/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "mutation.h"

Graphe* muterListe(Graphe* v, int nbGraphe, int proba, int* mesures_arc, Mesure mesures){
    srand(time(NULL));
    int i=0;
    for(i=0;i<nbGraphe;i++)
    {
        if((rand()%proba)==0) //Une chance sur proba de muter
        {
            v[i] = muter(v[i], mesures_arc, mesures);
        }
    }
    return v;
}

Graphe muter(Graphe vi, int* mesures_arc, Mesure mesures){
    srand(time(NULL));
    int a = rand()%(vi.M);
    int noMesure = rand()%mesures.nombreMesure;
    int rep;
    printf("La mesure \"%s\" sera applique a l'arc reliant le noeud %d au noeud %d. Donner une poderation(0-5) : ", mesures.nom[noMesure], vi.arcs[a].Nd, vi.arcs[a].Na);
    scanf("%d", rep);
    vi.arcs[a].val = rep;
    mesures_arc[a] = noMesure;
    return vi;
}