/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "genetique.h"

void setP(int ponderation) {
    P = ponderation;
}

void setBudgetCible(int budgetCible) {
    BUDGETCIBLE = budgetCible;
}

int valeur(Graphe graphe, Graphe graphe_depart, int* mesures_arc, Mesure mesures) {
    return (P * (longueur(graphe) / pcc(graphe_depart)) + (1 - P) * (BUDGETCIBLE / budgetTotal(graphe, mesures_arc, mesures)));
}