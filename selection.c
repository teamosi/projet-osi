/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "genetique.h"

Graphe* roueDeLaFortune(Graphe* G, Graphe graphe_depart, int nbGraphe, int* mesures_arc, Mesure mesures, int n) {
    if(nbGraphe <= n) return NULL;
    
    Graphe* res = (Graphe*)malloc(n*sizeof(Graphe));
    
    int sommeValeur = 0;
    int i;
    for(i = 0; i < nbGraphe; i++) {
        sommeValeur += valeur (G[i], graphe_depart, mesures_arc, mesures);
    }
    
    srand(time(NULL));
    int x = rand()%(sommeValeur+1);
    
    int nb = 0;
    int nbSelect = 0;
    
    while(nbSelect < n) {
        int sVal = 0, sValp = 0;
        for(i = 0; i < nbGraphe-nbSelect; i++) {
            //if(G[i] =! NULL) {
                sVal += valeur (G[i], graphe_depart, mesures_arc, mesures);
                if(i != nb) sValp += valeur (G[i], graphe_depart, mesures_arc, mesures);
            //}
        }

        if(sValp < x && x <= sVal) {
            res[nbSelect] = G[nb];
            G[nb] = G[nbGraphe-nbSelect-1];
            //G[nbGraphe-nbSelect-1] = NULL;
            nbSelect++;
        }
        nb = (nb+1)%(nbGraphe-nbSelect);
    }
    
    return res;
}