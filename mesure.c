/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "mesure.h"

Mesure* ajoutMesure(Mesure* mesures, char* nouveauNom, float nouveauCout) {
    Mesure* res = (Mesure*)malloc(sizeof(Mesure));
    res->nombreMesure = mesures->nombreMesure+1;
    res->nom = mesures->nom;
    res->cout = mesures->cout;
    res->nom = realloc(res->nom, (res->nombreMesure)*sizeof(char*));
    res->cout = realloc(res->cout, (res->nombreMesure)*sizeof(float));
    res->nom[res->nombreMesure-1] = nouveauNom;
    res->cout[res->nombreMesure-1] = nouveauCout;
    return res;
}

float getCout(Mesure* mesures, char* nom) {
    int i = 0;
    for(i = 0; i < mesures->nombreMesure; i++) {
        if(strcmp(mesures->nom[i], nom) == 0)
            return mesures->cout[i];
    }
    return 0;
}