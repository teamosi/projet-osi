#include "tests.h"

int main2(){
    Graphe M, P = test_graphe();
    
    M = creer_graphe(5);
    M = ajouter_arc(M, 0, 1, 40);
    M = ajouter_arc(M, 0, 2, 37);
    M = ajouter_arc(M, 0, 3, 120);
    M = ajouter_arc(M, 1, 2, 25);
    M = ajouter_arc(M, 3, 4, 48);
    M = ajouter_arc(M, 4, 2, 20);
    
    test_croisement(P, M);
    test_decomposition(P);
}